package com.example.project2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.Response.ErrorListener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;

import java.io.IOException;
import java.io.InputStream;
import java.net.ResponseCache;

public class MainActivity extends AppCompatActivity {

    private static final String URL = "http://www.mocky.io/v2/56fa31e0110000f920a72134";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        StringRequest request = new StringRequest(URL, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                Log.d("CODE",response);
//            }, new ErrorListener(){
//                @Override
//               public void onErrorReponse(VolleyError error){
//                    Toast.makeText(MainActivity.this,"Some",Toast.LENGTH_SHORT).show();
//                }
//            }
//        });
    }
    public void readJson (View v){
        String json_string = null;
        try{
            InputStream inputStream = getAssets().open("abc.json");
            int size = inputStream.available();
            byte[] buffer = new byte[size];
            inputStream.read(buffer);
            inputStream.close();

            json_string = new String(buffer,"UTF-8");

            Toast.makeText(getApplicationContext(),json_string,Toast.LENGTH_LONG).show();

            Gson gson = new Gson();

            User user = gson.fromJson(json_string,User.class);

            Toast.makeText(getApplicationContext(), (CharSequence) user.getCompany(),Toast.LENGTH_LONG).show();


        }catch (IOException e){
           e.printStackTrace();
        }
    }


}
